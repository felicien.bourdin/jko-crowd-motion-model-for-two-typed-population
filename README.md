# JKO crowd motion model for two typed population

Project realized by Félicien BOURDIN during his PhD Thesis.
The work of Félicien BOURDIN is supported by the ERC Grant NORIA.

Implementation of the adaptation of https://arxiv.org/abs/1502.06216 in the case of two species.

The codes JKO_1e.py (monotype population) and JKO_2e.py (multitype population) compute crowd motion models of the gradient flow type using regularized optimal transport.

Some pre-computed visuals are available in the subfolders.


## Demo notebook : ```demo.ipynb```
