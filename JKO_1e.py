import torch
import matplotlib.pyplot as plt
import time


def prox(p, sigma, D):
    ## See (4.3) de https://arxiv.org/pdf/1502.06216.pdf
    return torch.minimum(torch.ones_like(p),p*torch.exp(-sigma*D))

def crowd_motion_JKO_step(C,D,q, gamma,dt, eps = 10e-10, nitmax = 30):
    ## C : cost matrix
    ## D : objective function
    ## gamma : reg parameter
    ## q : density at previous timestep

    ## Implementation of https://arxiv.org/pdf/1502.06216.pdf
    ## JKO step with entropic regularized transport computed with Dykstra Iterations, see props 3.2 and eq (4.3).

    n = D.shape[0]
    # print(D)
    a = torch.ones(n)
    b = torch.ones(n)
    u = torch.ones(n)
    v = torch.ones(n)
    w = torch.ones(n)
    x = torch.ones(n)

    xi = torch.exp(-C/gamma)

    l=0
    while True:
        ## Iteration Dykstra
        # print(l%2)
        if l%2 == 0:
            at = a*w
            bt = q/((xi.T).matmul(at))
            # if torch.isnan(bt.any()):
            #     print(xi.matmul(at),'bt even')
            #     breakpoint()
            ut = w*a/at
            # if torch.isnan(ut.sum()):
            #     print( w, a, at,'ut even')
            #     breakpoint()
            vt = x*b/bt
            # if torch.isnan(vt.sum()):
            #     print(x,b,bt,'vt even')
            #     breakpoint()
        if l%2 == 1:
            bt = b*x
            # if torch.isnan(bt.sum()):
            #     print('bt odd', b ,x)
            #     breakpoint()
            at = prox(a*w*(xi.matmul(bt)), dt/gamma,D)/(xi.matmul(bt))
            # if torch.isnan(at.sum()):
            #     print('bt odd', b ,x)
            #     breakpoint()
            ut = w*a/at
            vt = x*b/bt

        x = v
        w = u
        u = ut
        v = vt
        a = at
        b = bt

        # print(b.shape, xi.shape, a.shape, q.shape)
        # print("cout",torch.linalg.norm(b*(xi.matmul(a))-q), " iteration ",l)
        # print("l",l)
        # print("b",b)
        # print("xi", xi.matmul(a))
        # print("a",a)
        # print("w",w)
        # breakpoint()
        l+=1

        ## Stopping criterion

        if torch.linalg.norm(b*(xi.matmul(a))-q) < eps:
            return prox(a*w*(xi.matmul(bt)), dt/gamma,D)
        if torch.isnan(torch.linalg.norm(b*(xi.matmul(a))-q)):
            raise ValueError('NaN cost')
        if l>nitmax:
            return prox(a*w*(xi.matmul(bt)), dt/gamma,D)

def crowd_motion_JKO(dt,nit,rho_i,f,gamma=.01, verbose = True):
    ## dt : timestep
    ## nit : number of timesteps
    ## rho_i : initial density
    ## f : potential function
    ## gamma : regularization parameter

    N=rho_i.shape[0]
    rho_hist = torch.zeros(N,N,nit+1)
    rho_hist[:,:,0] = rho_i[:,:]

    ## Computation of the field
    xx = (torch.arange(0,N,1)+1/2)/N
    XX,YY = torch.meshgrid(xx,xx)
    XX = XX.reshape((N*N,1))
    YY = YY.reshape((N*N,1))
    pos = torch.cat([XX,YY], dim = 1)
    cost_mat = torch.sum((pos[:, None, :] - pos[None, :, :]) ** 2, dim=2)

    ## Data has to be stored in line
    D = f(pos).reshape(N*N)
    q = rho_i.reshape(N*N)
    for k in range(nit):
        tps = time.time()
        # print(q.shape,D.shape, cost_mat.shape)
        q=crowd_motion_JKO_step(cost_mat,D,q,gamma,dt)
        rho_hist[:,:,k+1] = q.reshape((N,N))
        if verbose:
            print("Iteration ",k," over ", nit, ", time: ", time.time()-tps)
    return rho_hist

if __name__=='__main__':
    def f(x):
        return torch.sqrt((x[:,0]-1/2)**2 + (x[:,1]-1/2)**2)*10

    N=50
    dt=.01
    nit=10
    rho = torch.outer(torch.arange(0,N,1),torch.arange(0,N,1))+1
    rho = rho/rho.max()
    rho_hist = crowd_motion_JKO(dt,nit,rho,f,gamma=.01)

    plt.figure(figsize=(30,20))
    plt.subplot(241)
    plt.imshow(rho_hist[:,:,0])
    plt.colorbar()
    plt.title('time: '+ str(0))
    plt.subplot(242)
    plt.imshow(rho_hist[:,:,int(nit/7)])
    plt.colorbar()
    plt.title('time: '+ str(int(nit/7)*dt))
    plt.subplot(243)
    plt.imshow(rho_hist[:,:,int(2*nit/7)])
    plt.colorbar()
    plt.title('time: '+ str(int(2*nit/7)*dt))
    plt.subplot(244)
    plt.imshow(rho_hist[:,:,int(3*nit/7)])
    plt.colorbar()
    plt.title('time: '+ str(int(3*nit/7)*dt))
    plt.subplot(245)
    plt.imshow(rho_hist[:,:,int(4*nit/7)])
    plt.colorbar()
    plt.title('time: '+ str(int(4*nit/7)*dt))
    plt.subplot(246)
    plt.imshow(rho_hist[:,:,int(5*nit/7)])
    plt.colorbar()
    plt.title('time: '+ str(int(5*nit/7)*dt))
    plt.subplot(247)
    plt.imshow(rho_hist[:,:,int(6*nit/7)])
    plt.colorbar()
    plt.title('time: '+ str(int(6*nit/7)*dt))
    plt.subplot(248)
    plt.imshow(rho_hist[:,:,int(7*nit/7)])
    plt.title('time: '+ str(int(7*nit/7)*dt))
    plt.colorbar()

    plt.show()