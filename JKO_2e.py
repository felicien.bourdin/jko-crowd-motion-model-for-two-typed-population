import torch
import matplotlib.pyplot as plt
import time

def prox(p1,p2,D1,D2, sigma):
    ## Computation of the proximal operator for two types, see https://arxiv.org/abs/1502.06216
    sum = p1*torch.exp(-sigma*D1) +  p2*torch.exp(-sigma*D2)
    return  p1*torch.exp(-sigma*D1)*torch.minimum(sum, torch.ones_like(sum))/sum, p2*torch.exp(-sigma*D2)*torch.minimum(sum,torch.ones_like(sum))/sum

def crowd_motion_JKO_step(C,D1,D2,q1,q2, gamma,dt, eps = 10e-10, nitmax = 30):
    ## JKO step
    ## C is the cost matrix,
    ## D1 is the potential for type 1
    ## D2 is the potential for type 2
    ## q1 and q2 are the densities at step n-1
    ## gamma is the regularisation parameter
    ## dt is the timesetp
    ## eps is a stopping criterion for Dysktra iterations
    ## nit max is the maximal number of Dysktra iterations

    n = D1.shape[0]
    # print(D)
    a1 = torch.ones(n)
    b1 = torch.ones(n)
    u1 = torch.ones(n)
    v1 = torch.ones(n)
    w1 = torch.ones(n)
    x1 = torch.ones(n)
    a2 = torch.ones(n)
    b2 = torch.ones(n)
    u2 = torch.ones(n)
    v2 = torch.ones(n)
    w2 = torch.ones(n)
    x2 = torch.ones(n)

    xi = torch.exp(-C / gamma)

    l = 0
    while True:
        ## Iteration Dykstra
        # print(l%2)
        if l%2 == 0:
            at1 = a1*w1
            bt1 = q1/((xi.T).matmul(at1))
            # if torch.isnan(bt.any()):
            #     print(xi.matmul(at),'bt even')
            #     breakpoint()
            ut1 = w1*a1/at1
            # if torch.isnan(ut.sum()):
            #     print( w, a, at,'ut even')
            #     breakpoint()
            vt1 = x1*b1/bt1
            # if torch.isnan(vt.sum()):
            #     print(x,b,bt,'vt even')
            #     breakpoint()
            at2 = a2*w2
            bt2 = q2/((xi.T).matmul(at2))
            # if torch.isnan(bt.any()):
            #     print(xi.matmul(at),'bt even')
            #     breakpoint()
            ut2 = w2*a2/at2
            # if torch.isnan(ut.sum()):
            #     print( w, a, at,'ut even')
            #     breakpoint()
            vt2 = x2*b2/bt2
            # if torch.isnan(vt.sum()):
            #     print(x,b,bt,'vt even')
            #     breakpoint()

        if l%2 == 1:
            bt1 = b1*x1
            bt2 = b2*x2
            # if torch.isnan(bt.sum()):
            #     print('bt odd', b ,x)
            #     breakpoint()
            prox1,prox2 = prox(a1*w1*(xi.matmul(bt1)),a2*w2*(xi.matmul(bt2)),D1,D2, dt/gamma)
            at1 = prox1/(xi.matmul(bt1))
            at2 = prox2/(xi.matmul(bt2))

            # if torch.isnan(at.sum()):
            #     print('bt odd', b ,x)
            #     breakpoint()
            ut1 = w1*a1/at1
            vt1 = x1*b1/bt1
            ut2 = w2 * a2 / at2
            vt2 = x2 * b2 / bt2

        x1 = v1
        w1 = u1
        u1 = ut1
        v1 = vt1
        a1 = at1
        b1 = bt1

        x2 = v2
        w2 = u2
        u2 = ut2
        v2 = vt2
        a2 = at2
        b2 = bt2

        l+=1

        ## Stopping criterions

        if torch.linalg.norm(b1*(xi.matmul(a1))-q1) + torch.linalg.norm(b2*(xi.matmul(a2))-q2)< eps:
            return prox(a1*w1*(xi.matmul(bt1)),a2*w2*(xi.matmul(bt2)),D1,D2, dt/gamma)
        if torch.isnan(torch.linalg.norm(b1*(xi.matmul(a1))-q1) + torch.linalg.norm(b2*(xi.matmul(a2))-q2)):
            raise ValueError('NaN cost')
        if l>nitmax:
            return prox(a1*w1*(xi.matmul(bt1)),a2*w2*(xi.matmul(bt2)),D1,D2, dt/gamma)

def crowd_motion_JKO(dt,nit,rho_i_1,rho_i_2,f1,f2,gamma=.01, verbose = True):
    ## dt : timestep
    ## nit : number of timestep
    ## rho_i : initial densities
    ## f_i : potential functions
    ## gamma : entropic regularisation parameter

    N=rho_i_1.shape[0]
    rho_hist = torch.zeros(N,N,nit+1,2)
    rho_hist[:,:,0,0] = rho_i_1[:,:]
    rho_hist[:,:,0,1] = rho_i_2[:,:]

    ## Computation of the fields

    xx = (torch.arange(0,N,1)+1/2)/N
    XX,YY = torch.meshgrid(xx,xx)
    XX = XX.reshape((N*N,1))
    YY = YY.reshape((N*N,1))
    pos = torch.cat([XX,YY], dim = 1)
    cost_mat = torch.sum((pos[:, None, :] - pos[None, :, :]) ** 2, dim=2)

    ## Data has to be stored in line

    D1 = f1(pos).reshape(N*N)
    D2 = f2(pos).reshape(N*N)
    q1 = rho_i_1.reshape(N*N)
    q2 = rho_i_2.reshape(N*N)
    for k in range(nit):
        tps = time.time()
        q1,q2=crowd_motion_JKO_step(cost_mat,D1,D2,q1,q2,gamma,dt)
        rho_hist[:,:,k+1,0] = q1.reshape((N,N))
        rho_hist[:,:,k+1,1] = q2.reshape((N,N))
        if verbose:
            print("Iteration ",k," over ", nit, ", time: ", time.time()-tps, ", sum 1 : ", q1.sum().item(), ", sum 2 : ", q2.sum().item())
    return rho_hist

if __name__=="__main__":

    ## Potentials

    def f1(x):
        return torch.sqrt(((x[:,0]-1/2)**2 + (x[:,1]-1/2)**2))*5


    def f2(x):
        return -((x[:, 0] - 1 / 2) ** 2 + (x[:, 1] - 1 / 2) ** 2)*10

    ## Simulation parameters

    N=50
    dt=.02
    nit=10

    ## Initial conditions (discs)

    rho1 = torch.ones((N,N))*0.001
    rho2 = torch.ones((N,N))*0.001

    for i in range(N):
        for j in range(N):
            if (i/N-.5)**2 + (j/N-.5)**2 < .4**2:
                rho1[i,j] = .5
            if (i/N-.5)**2 + (j/N-.5)**2 < .4**2:
                rho2[i,j] = .5



    # print((rho1+rho2).max())

    ## Run the model

    rho_hist_tot = crowd_motion_JKO(dt,nit,rho1,rho2,f1,f2,gamma=.01)

    ## Display results

    rho_hist = rho_hist_tot[:,:,:,0]
    plt.figure(figsize=(30, 20))
    plt.suptitle("rho_1")
    plt.subplot(241)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, 0], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(0))
    plt.subplot(242)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(nit / 7) * dt))
    plt.subplot(243)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(2 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(2 * nit / 7) * dt))
    plt.subplot(244)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(3 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(3 * nit / 7) * dt))
    plt.subplot(245)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(4 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(4 * nit / 7) * dt))
    plt.subplot(246)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(5 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(5 * nit / 7) * dt))
    plt.subplot(247)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(6 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.axis('off')
    plt.title('time: ' + str(int(6 * nit / 7) * dt))
    plt.subplot(248)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(7 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.title('time: ' + str(int(7 * nit / 7) * dt))
    plt.colorbar()

    rho_hist = rho_hist_tot[:,:,:,1]
    plt.figure(figsize=(30,20))
    plt.suptitle("rho_2")
    plt.subplot(241)
    plt.axis('off')
    plt.imshow(rho_hist[:,:,0], vmin=0, vmax=1, cmap="Greys")
    plt.colorbar()
    plt.title('time: '+ str(0))
    plt.subplot(242)
    plt.axis('off')
    plt.imshow(rho_hist[:,:,int(nit/7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: '+ str(int(nit/7)*dt))
    plt.subplot(243)
    plt.axis('off')
    plt.imshow(rho_hist[:,:,int(2*nit/7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: '+ str(int(2*nit/7)*dt))
    plt.subplot(244)
    plt.axis('off')
    plt.imshow(rho_hist[:,:,int(3*nit/7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: '+ str(int(3*nit/7)*dt))
    plt.subplot(245)
    plt.axis('off')
    plt.imshow(rho_hist[:,:,int(4*nit/7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: '+ str(int(4*nit/7)*dt))
    plt.subplot(246)
    plt.axis('off')
    plt.imshow(rho_hist[:,:,int(5*nit/7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: '+ str(int(5*nit/7)*dt))
    plt.subplot(247)
    plt.axis('off')
    plt.imshow(rho_hist[:,:,int(6*nit/7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: '+ str(int(6*nit/7)*dt))
    plt.subplot(248)
    plt.axis('off')
    plt.imshow(rho_hist[:,:,int(7*nit/7)], vmin=0, vmax=1, cmap= "Greys")
    plt.title('time: '+ str(int(7*nit/7)*dt))
    plt.colorbar()

    rho_hist = rho_hist_tot[:, :, :, 0]+rho_hist_tot[:, :, :, 1]
    plt.figure(figsize=(30, 20))
    plt.suptitle("rho_tot")
    plt.subplot(241)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, 0], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(0))
    plt.subplot(242)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(nit / 7) * dt))
    plt.subplot(243)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(2 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(2 * nit / 7) * dt))
    plt.subplot(244)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(3 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(3 * nit / 7) * dt))
    plt.subplot(245)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(4 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(4 * nit / 7) * dt))
    plt.subplot(246)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(5 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(5 * nit / 7) * dt))
    plt.subplot(247)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(6 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.colorbar()
    plt.title('time: ' + str(int(6 * nit / 7) * dt))
    plt.subplot(248)
    plt.axis('off')
    plt.imshow(rho_hist[:, :, int(7 * nit / 7)], vmin=0, vmax=1, cmap= "Greys")
    plt.title('time: ' + str(int(7 * nit / 7) * dt))
    plt.colorbar()

    plt.show()